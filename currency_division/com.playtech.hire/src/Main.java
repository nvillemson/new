import java.math.BigDecimal;
import java.math.RoundingMode;

public class Main {

    // TODO
    public static void main (String[] args) throws Exception {
        // test 1
        calculate(4, 2, 1);

        // test 2
        calculate(10, 7, 2);

        // test3
        calculate(2, 11,3);
    }

    public static void calculate(int money, int people, int testNumber) {
        BigDecimal money2 = BigDecimal.valueOf(money).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal people2 = BigDecimal.valueOf(people).setScale(2, RoundingMode.HALF_EVEN);

        System.out.println("Test " + testNumber);

        BigDecimal value = money2.divide(people2, 2, RoundingMode.HALF_EVEN);
        for (int i = 0; i < people; i ++) {
            System.out.println(value); // second test case
        }

        System.out.println();
    }
}
