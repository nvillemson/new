package com.playtech.hire.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;

import com.playtech.hire.auth.Accounts;

public class CreateInitial implements Callable<Accounts>{

  
  @Override
  public Accounts call() throws Exception {
    return createInitial();
  }

  private static Accounts createInitial() throws IOException {
    Accounts acc = new Accounts();
    File initialFile = new File("initial.acc");
    
    try(InputStream fileStream = new FileInputStream(initialFile)){
      acc.getStorage().read(fileStream);
      if (acc.size() > 0){
        Assert.assertEquals(acc.auth("xxs", "noPass0").getUid(), "xxs");
        return acc;
      }
    } catch(FileNotFoundException _skip){}

    acc.create("xxs", "noPass0");
    acc.create("bin", "-txtBin");
    
    try(OutputStream out = new FileOutputStream(initialFile)){
      acc.getStorage().write(out);
    }
    return acc;
  }
}
