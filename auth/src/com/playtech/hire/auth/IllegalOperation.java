package com.playtech.hire.auth;

public class IllegalOperation extends RuntimeException{

  public IllegalOperation(String message, Object... args) {
    super(String.format(message, args));
  }

}
