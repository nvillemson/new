package com.playtech.hire.auth;

import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

/*
Managed to fix several things included some tests, some naming convention
At least some serious bugs
Still self made asserts and tests with main is not the best idea.
Code is a bit better but still looks a bit like labyrinth. But that requires rewriting everything.
isEqual method bug with String comparison was one of the worst
 */
public class Accounts {
  final SortedMap<String, String> auth = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

  // seems used only in test classes, at least this method calls methods that are only used in classes under package test
  void addExistingEntry(String uid, String storedPassword) {
    auth.putIfAbsent(validateUid(uid), Objects.requireNonNull(storedPassword));
  }

  /**
   * 
   * @param uid user identity
   * @param plainTextPassword plaintext password of the uid
   * @return exact uid of the account when authentication succeeds, null when no account found
   */
  public LoginEntry auth(String uid, String plainTextPassword){
    String pwd = auth.get(uid);
    if (pwd == null){
      throw new IllegalOperation("Account [%s] not found", uid);
    }

    LoginEntry userEntry = new LoginEntry(uid, pwd);
    if (userEntry.matchesPassword(plainTextPassword)) {
      return new LoginEntry(auth.tailMap(uid).firstKey()//exact matching
          , pwd);
    } else {
      throw new IllegalOperation("Wrong password.", uid);
    }
  }

  /**
   * 
   * @param uid
   * @param plainTextPassword
   * @return a newly create account
   * @throws IllegalOperation if the account exist already or the input is illegal 
   */
  public LoginEntry create(String uid, String plainTextPassword){
    LoginEntry e = newLoginEntry(uid, plainTextPassword);
    if (e.addTo(auth)) {
      return e;
    } else {
      throw new IllegalOperation("Account already exists");
    }
  }

  private static String validateUid(String uid) {
    String banned = "[]{}<>|*?&";
    for (char c : uid.toCharArray()){//null check as well
      if (Character.isWhitespace(c)){
        throw new IllegalOperation("A white space character in uid");
      }
      if (Character.isISOControl(c) || Character.isMirrored(c) || Character.isLowSurrogate(c) || Character.isHighSurrogate(c)){
        throw new IllegalOperation("Unsupported character");
      }
      if (banned.indexOf(c) >= 0){
        throw new IllegalOperation("Unsupported character in uid: %s", c);
      }
    }
    return uid;
  }

  private LoginEntry newLoginEntry(String uid, String plainTextPassword) {    
    if (plainTextPassword.isEmpty()){
      throw new IllegalOperation("Empty password");
    }
    
    if (plainTextPassword.trim() != plainTextPassword){
      throw new IllegalOperation("Whitespaces at the begging/end of password");
    }

    LoginEntry e = new LoginEntry(validateUid(uid), plainTextPassword);
    return e;
  }

  // FOR THE TESTS
  public SortedMap<String, String> getAuth() {
    return auth;
  }

  public LoginEntry changePassword(String uid, String oldPassword, String newPassword){
    LoginEntry existing = auth(uid, oldPassword);
    LoginEntry replacement = newLoginEntry(existing.getUid(), newPassword);
    replacement.replaceExisting(auth);
    return replacement;    
  }
  
  public Storage getStorage(){
    return new Storage(this);
  }

  public int size() {
    return auth.size();
  }

}
