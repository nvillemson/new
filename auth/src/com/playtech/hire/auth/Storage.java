package com.playtech.hire.auth;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Storage {
  
  private final Accounts accounts;

  Storage(Accounts accounts){
    this.accounts = accounts;
  }

  // read only used in tests as I have found out, well at least in the test package
  public void read(InputStream in) throws IOException{
    Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
    Properties prop = new Properties();
    prop.load(reader);

    prop.forEach((k,v) -> accounts.addExistingEntry(k.toString(), v.toString()));
  }

  public void write(OutputStream out) throws IOException{   
    Properties prop = new Properties(){//ordered storage (alphabetically)
      @Override
      public synchronized Enumeration<Object> keys() {
        Object[] keys = accounts.auth.keySet().toArray(); // bad naming convention, not only here
        return Collections.enumeration(Arrays.asList(keys));
      }
    };

    prop.putAll(accounts.auth);
    prop.store(new OutputStreamWriter(out, StandardCharsets.UTF_8), "Accounts store as java properties");
  }
}
